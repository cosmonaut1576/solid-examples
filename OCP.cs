//Wrong

class Order{
	public string PositionofMenu{ get; set; }
	public int NumberOfPortion { get; set; }
}

class KitchenWork
{
    public void Cook (Order order) 
    {    
         //приготовление блюд
    } 
}

class ClientService
{
	public Order SetOrder(Object object)
	{
		if (Object.GetName(object).Equals(Client)) then 
		{
			//сделать заказ
		}
		if (Object.GetName(object).Equals(VIPClient)) then ..
		{
			//сделать заказ с использованием бонусов/скидок
		}
	}
	
	
}


//Right

class Order{
	public string PositionofMenu{ get; set; }
	public int NumberOfPortion { get; set; }
}

class KitchenWork
{
    public void Cook (Order order) 
    {    
         //приготовление блюд
    } 
}

interface IClientService
{
	void SetOrder();
}

class ClientService : IClientService
{
	public Order SetOrder()
	{
		//сделать заказ
	}
}

class VIPClientService : IClientService
{
	public Order SetOrder()
	{
		//сделать заказ с использованием бонусов/скидок
	}
}


