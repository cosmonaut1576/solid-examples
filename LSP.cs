//Wrong

class KitchenWork
{
    public dish Cook (Order order) 
    {    
         //приготовление блюд
    } 
}

class VIPKitchenWork : KitchenWork
{
	public dish Cook (Order order) 
    {    
         //приготовление блюд молекулярной кухни
    } 
}

//test
public class UnitTest
    {
        [Fact]
        public void ReturnBorsch()
        {
            var KW = new KitchenWork();
			var order = new Order{PositionofMenu = "Borsch",NumberOfPortion = 2};
            var dish = KW.Cook(order);

            Assert.Equal(dish, "Borsch");
        }

        [Fact]
        public void ReturnBorschFromVIP()
        {
            var VIPkw = new VIPKitchenWork();
			var order = new Order{PositionofMenu = "Borsch",NumberOfPortion = 2};
            var dish = VIPkw.Cook(order);

            Assert.Equal(dish, "Borsch");//fail
        }
	}


//Right

interface IKitchenWork
{
	dish Cook(order);
}

class SimpleKitchenWork : IKitchenWork
{
    public void Cook (order) 
    {    
         //приготовление блюд
    } 
}

class VIPKitchenWork : IKitchenWork
{
	public dish Cook (order) 
    {    
         //приготовление блюд молекулярной кухни
    } 
}



