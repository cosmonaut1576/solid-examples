//Wrong

class Order{
	public string PositionofMenu{ get; set; }
	public int NumberOfPortion { get; set; }
}

class KitchenWork
{
    public void Cook (Order order) 
    {    
         //приготовление 
    } 

	public Order SetOrder()
	{
		//принятие заказа
	}

}

class Client
{
	public void EatFood();
}

class Cafe
{   
    static void Main() 
    {   
		Client client = new Client();
        KitchenWork kw = new KitchenWork();
        Order order = new Order();
		order = kw.ServiceClient();
		client.EatFood(kw.Cook(order));
		  
    }
}

//Right

class Order{
	public string PositionofMenu{ get; set; }
	public int NumberOfPortion { get; set; }
}

class KitchenWork
{
    public void Cook (Order order) 
    {    
         //приготовление 
    } 
}

class ClientService
{
	public Order SetOrder()
	{
		//принятие заказа
	}
}

class Client
{
	public void EatFood();
}

class Cafe
{   
    static void Main() 
    {   
		Client client = new Client();
		Order order = new Order();
        KitchenWork kw = new KitchenWork();
		ClientService clientService = new ClientService();
		order = clientService.SetOrder();
		client.EatFood(kw.Cook(order));
		  
    }
}