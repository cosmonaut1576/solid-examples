//Wrong

class Kitchen
{
	public Order order { get; set; }
	public RussianKitchen russianKitchen { get; set; }
	
    public readyOrder Cook () 
    {    
         russianKitchen.Cook(order);//приготовление блюд
    } 
}

class RussianKitchen
{
	public dish Cook (order) 
    {    
         //готовим борщ/блины/пельмени/голубцы/компот
    } 	
}

//Right

interface IKitchenWork
{
	bool Cook (Order order);  //приготовление блюд
}

class Kitchen
{
	public Order order { get; set; }
	public IKitchenWork KitchenWork { get; set; }
	
    public Kitchen(IKitchenWork kitchenWork)
    {    
        this.KitchenWork = kitchenWork;
    } 
	
	public readyOrder Cook () 
    {    
        KitchenWork.Cook(order);//приготовление блюд
    } 
}

class RussianKitchen
{
	public dish Cook (order) 
    {    
         //готовим борщ/блины/пельмени/голубцы/компот
    } 	
}

class ItalianKitchen
{
	public dish Cook (order) 
    {    
         //готовим пицца/спагетти/лазанья/лимончелло
    } 	
}

Kitchen kitchen = new Kitchen(new ItalianKitchen());
kitchen.Order = new Order { PositionofMenu = "Pizza", NumberOfPortion = 1 };
bool firstOrder =  kitchen.Cook();
kitchen.KitchenWork = new RussianKitchen();
kitchen.Order = new Order { PositionofMenu = "Borsch", NumberOfPortion = 2 };
bool secondOrder =  kitchen.Cook();