//Wrong

interface IKitchen
{
	int ChooseTable();
    Order SetOrder(int numberOfTable);
	float SetPrice(Menu menu, Order order)
	bool ProcessOrder(Order order);
    bool Cook (Order order);  //приготовление блюд
	bool WashUp(Dishes dirtyDishes);
	payment EatFood(bool readyOrder);
}


//Right

interface IKitchenWork
{
	bool ProcessOrder(Order order);
    bool Cook (Order order);  //приготовление блюд
}

interface ICleaning
{
	bool WashUp(Dishes dirtyDishes);
}

interface IServeClient
{
	float SetPrice(Menu menu, Order order)
	bool ProcessOrder(Order order);
}

interface IServeClient
{
	int ChooseTable();
	payment EatFood(bool readyOrder);
}



